# Palo Alto Networks User-ID Installation

The User-ID agent is a mechanism for mapping usernames to ip addresses. The
User-ID agent runs as a low privilege user to monitor login events on your
Active Directory infrastructure. 

This document will show you how to install the agent and configure the agent
for the principal of least privilege.

## **Installation**

Download the msi file here:

### **Run the installation msi**
Run the MSI file on the server and click next through all the screens. 

Once you have installed the User-ID Agent we can now configure.

### **Add service user to Active Directory**

Now we will add a user to active directory, you can call this user anything you
would like but I generally add "proxy" to the end of the user name to denote
a user that is used for a system. I use the word proxy because it does not
exist anywhere else and is easily searchable. 

Open "Active Directory Users and Computers" (or CTRL+R and type dsa.msc) and expand your domain. Right click
on "Managed Service Accounts" and go to New -> User. 

Once you have filled out the form for the new user, right click the user and
select "Properties" and click on the "Member Of" tab. 

### **Add service user to appropriate groups**

Now that we have added the service user, select the user, right click and
select "Properties" and click the "Member Of" tab.

This user will need to belong to the following groups: 

- Distributed COM Users
- Domain Users
- Event Log Readers

### **Registry Settings**

In this case we are utilizing the principal of least privilege so we will need
to manually change registry settings.   

Open up regedit by hitting CTRL+R and typing "regedit"  

Once open browse to: ``Computer -> HKEY_LOCAL_MACHINE -> SOFTWARE -> Wow6432Node -> Palo
Alto Networks``  

Right click "Palo Alto Networks" and click "Permissions". The user we created
above will need full access to this registry hive. 

### Service Configuration

Hit the keyboard combination CTRL+R and type "services.msc"  

Find "User-ID Agent" and right click to select "Properties"  

Next click the "Log On" tab and select "This account"  

You can either type in the name of the account you created above or hit browse
to find the user. Enter the password and hit "Apply"  

Click "OK" and exit the screen.  

Right click the service and select "Start" or "Restart" if the service is
already running.  

### User-ID Agent Configuration

Click the start menu and search the term "User-ID Agent". This is the
configuration utility for the Palo Alto Networks User-ID Agent. 

Open the configuration utility and click "Setup" under User Identification then
click "Edit"

Verify the "Client Probing" **does not** have check marks next to:

- Enable WMI Probing
- Enable NetBIOS Probing

Click OK to exit. 

Verify your servers are listed under User Identification -> Discovery. If
they are not listed, click "Auto Discover". If this does not show all servers,
you can manually add the others by clicking "Add"
